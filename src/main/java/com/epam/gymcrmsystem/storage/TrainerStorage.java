package com.epam.gymcrmsystem.storage;

import com.epam.gymcrmsystem.entity.Trainer;
import com.epam.gymcrmsystem.entity.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author Arpi Khachatryan on 19.05.2024
 */

@Slf4j
public class TrainerStorage {

    private UserStorage userStorage;

    @Autowired
    public void setUserStorage(UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    @Value("${data.file.trainer.path}")
    private String dataFilePath;

    private final Map<Long, Trainer> trainerMap = new HashMap<>();

    private long currentId = 0;

    @PostConstruct
    public void init() {
        loadFromFile();
    }

    public Trainer save(Trainer trainer) {
        trainer.setId(++currentId);
        trainerMap.put(trainer.getId(), trainer);
        saveToFile();
        log.info("Trainer saved: {}", trainer);
        return trainer;
    }

    public Trainer update(Trainer trainer) {
        Long trainerId = trainer.getId();
        Trainer existingTrainer = trainerMap.get(trainerId);
        if (existingTrainer != null) {
            existingTrainer.setSpecialization(trainer.getSpecialization());
            existingTrainer.setUser(trainer.getUser());
            if (trainer.getUser() != null) {
                User updatedUser = userStorage.update(trainer.getUser());
                existingTrainer.setUser(updatedUser);
            }
            saveToFile();
            log.info("Trainer updated: {}", existingTrainer);
            return existingTrainer;
        } else {
            log.warn("Trainer with ID {} not found for update", trainerId);
            return null;
        }
    }

    public Optional<Trainer> findById(Long id) {
        Trainer foundTrainer = trainerMap.get(id);
        if (foundTrainer != null) {
            log.info("Trainer found by id {}: {}", id, foundTrainer);
            return Optional.of(foundTrainer);
        } else {
            log.warn("Trainer with id {} not found", id);
            return Optional.empty();
        }
    }

    public List<Trainer> findAll() {
        List<Trainer> allTrainers = new ArrayList<>(trainerMap.values());
        log.info("Retrieved all trainers: {}", allTrainers);
        return allTrainers;
    }

    private void loadFromFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Trainer> trainers = objectMapper.readValue(Paths.get(dataFilePath).toFile(), new TypeReference<List<Trainer>>() {
            });
            trainers.forEach(trainer -> {
                trainerMap.put(trainer.getId(), trainer);
                currentId = Math.max(currentId, trainer.getId());
            });
            log.info("Loaded trainers from file: {}", trainers);
        } catch (IOException e) {
            log.error("Failed to load trainers from file: {}", e.getMessage());
        }
    }

    private void saveToFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(Paths.get(dataFilePath).toFile(), trainerMap.values());
            log.info("Saved trainers to file");
        } catch (IOException e) {
            log.error("Failed to save trainers to file: {}", e.getMessage());
        }
    }
}