package com.epam.gymcrmsystem.storage;

import com.epam.gymcrmsystem.entity.Training;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
public class TrainingStorage {

    @Value("${data.file.training.path}")
    private String dataFilePath;

    private final Map<Long, Training> trainingMap = new HashMap<>();

    private long currentId = 0;

    @PostConstruct
    public void init() {
        loadFromFile();
    }

    public Training save(Training training) {
        training.setId(++currentId);
        trainingMap.put(training.getId(), training);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date trainingDate = simpleDateFormat.parse(String.valueOf(training.getTrainingDate()));
            training.setTrainingDate(trainingDate);
        } catch (ParseException e) {
            log.error("Failed to parse date of birth: {}", e.getMessage());
        }
        saveToFile();
        log.info("Saved training with ID: {}", training.getId());
        return training;
    }

    public Optional<Training> findById(Long id) {
        Training training = trainingMap.get(id);
        if (training != null) {
            log.info("Found training by ID {}: {}", id, training);
            return Optional.of(training);
        } else {
            log.warn("Training with ID {} not found", id);
            return Optional.empty();
        }
    }

    public List<Training> findAll() {
        List<Training> allTrainings = new ArrayList<>(trainingMap.values());
        log.info("Retrieved all trainings: {}", allTrainings);
        return allTrainings;
    }

    public boolean existsByTrainerIdAndTraineeId(Long trainerId, Long traineeId) {
        return trainingMap.values().stream()
                .anyMatch(training -> training.getTrainer().getId().equals(trainerId) && training.getTrainee().getId().equals(traineeId));
    }

    private void loadFromFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Training> trainings = objectMapper.readValue(Paths.get(dataFilePath).toFile(), new TypeReference<List<Training>>() {
            });
            trainings.forEach(training -> {
                trainingMap.put(training.getId(), training);
                currentId = Math.max(currentId, training.getId());
            });
            log.info("Loaded {} trainings from file.", trainings.size());
        } catch (IOException e) {
            log.error("Failed to load trainings from file: {}", e.getMessage());
        }
    }

    private void saveToFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(Paths.get(dataFilePath).toFile(), trainingMap.values());
            log.info("Saved all trainings to file.");
        } catch (IOException e) {
            log.error("Failed to save trainings to file: {}", e.getMessage());
        }
    }
}