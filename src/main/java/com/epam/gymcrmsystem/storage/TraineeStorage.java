package com.epam.gymcrmsystem.storage;

import com.epam.gymcrmsystem.entity.Trainee;
import com.epam.gymcrmsystem.entity.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
public class TraineeStorage {

    private UserStorage userStorage;

    @Autowired
    public void setUserStorage(UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    @Value("${data.file.trainee.path}")
    private String dataFilePath;

    private final Map<Long, Trainee> traineeMap = new HashMap<>();

    private long currentId = 0;

    @PostConstruct
    public void init() {
        loadFromFile();
    }

    public Trainee save(Trainee trainee) {
        trainee.setId(++currentId);
        traineeMap.put(trainee.getId(), trainee);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dateOfBirth = simpleDateFormat.parse(String.valueOf(trainee.getDateOfBirth()));
            trainee.setDateOfBirth(dateOfBirth);
        } catch (ParseException e) {
            log.error("Failed to parse date of birth: {}", e.getMessage());
        }

        saveToFile();
        log.info("Trainee saved: {}", trainee);
        return trainee;
    }

    public Trainee update(Trainee trainee) {
        Long traineeId = trainee.getId();
        Trainee existingTrainee = traineeMap.get(traineeId);
        if (existingTrainee != null) {
            existingTrainee.setAddress(trainee.getAddress());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date dateOfBirth = sdf.parse(String.valueOf(trainee.getDateOfBirth()));
                existingTrainee.setDateOfBirth(dateOfBirth);
            } catch (ParseException e) {
                log.error("Failed to parse date of birth: {}", e.getMessage());
            }

            if (trainee.getUser() != null) {
                User updatedUser = userStorage.update(trainee.getUser());
                existingTrainee.setUser(updatedUser);
            }

            saveToFile();
            log.info("Trainee updated: {}", existingTrainee);
            return existingTrainee;
        } else {
            log.warn("Trainee with ID {} not found for update", traineeId);
            return null;
        }
    }

    public void delete(Long id) {
        Trainee removedTrainee = traineeMap.remove(id);
        saveToFile();
        if (removedTrainee != null) {
            log.info("Trainee deleted: {}", removedTrainee);
        } else {
            log.warn("Trainee with id {} not found for deletion", id);
        }
    }

    public Optional<Trainee> findById(Long id) {
        Trainee foundTrainee = traineeMap.get(id);
        if (foundTrainee != null) {
            log.info("Trainee found by id {}: {}", id, foundTrainee);
            return Optional.of(foundTrainee);
        } else {
            log.warn("Trainee with id {} not found", id);
            return Optional.empty();
        }
    }

    public List<Trainee> findAll() {
        List<Trainee> allTrainees = new ArrayList<>(traineeMap.values());
        log.info("Retrieved all trainees: {}", allTrainees);
        return allTrainees;
    }

    private void loadFromFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Trainee> trainees = objectMapper.readValue(Paths.get(dataFilePath).toFile(), new TypeReference<List<Trainee>>() {
            });
            trainees.forEach(trainee -> {
                traineeMap.put(trainee.getId(), trainee);
                currentId = Math.max(currentId, trainee.getId());
            });
            log.info("Loaded trainees from file: {}", trainees);
        } catch (IOException e) {
            log.error("Failed to load trainees from file: {}", e.getMessage());
        }
    }

    private void saveToFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(Paths.get(dataFilePath).toFile(), traineeMap.values());
            log.info("Saved trainees to file");
        } catch (IOException e) {
            log.error("Failed to save trainees to file: {}", e.getMessage());
        }
    }
}