package com.epam.gymcrmsystem.storage;

import com.epam.gymcrmsystem.entity.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Arpi Khachatryan on 19.05.2024
 */

@Slf4j
public class UserStorage {

    @Value("${data.file.user.path}")
    private String dataFilePath;

    private final Map<Long, User> userMap = new HashMap<>();

    private long currentId = 0;

    @PostConstruct
    public void init() {
        loadFromFile();
    }

    public User save(User user) {
        user.setId(++currentId);
        userMap.put(user.getId(), user);
        saveToFile();
        log.info("Saved user with ID: {}", user.getId());
        return user;
    }

    public User update(User user) {
        userMap.put(user.getId(), user);
        saveToFile();
        log.info("Updated user with ID: {}", user.getId());
        return user;
    }

    public boolean existsByUsername(String username) {
        return userMap.values().stream().anyMatch(user -> user.getUserName().equals(username));
    }

    private void loadFromFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<User> users = objectMapper.readValue(Paths.get(dataFilePath).toFile(), new TypeReference<List<User>>() {
            });
            users.forEach(user -> {
                userMap.put(user.getId(), user);
                currentId = Math.max(currentId, user.getId());
            });
            log.info("Loaded {} users from file.", users.size());
        } catch (IOException e) {
            log.error("Failed to load users from file: {}", e.getMessage());
        }
    }

    private void saveToFile() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(Paths.get(dataFilePath).toFile(), userMap.values());
            log.info("Saved all users to file.");
        } catch (IOException e) {
            log.error("Failed to save users to file: {}", e.getMessage());
        }
    }
}