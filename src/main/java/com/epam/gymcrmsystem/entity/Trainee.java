package com.epam.gymcrmsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trainee {
    private Long id;
    private Date dateOfBirth;
    private String address;
    private User user;
}