package com.epam.gymcrmsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trainer {
    private Long id;
    private String specialization;
    private User user;
}