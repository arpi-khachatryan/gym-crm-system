package com.epam.gymcrmsystem.entity;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public enum TrainingType {
    ENDURANCE, BALANCE, FLEXIBILITY, STRENGTH
}