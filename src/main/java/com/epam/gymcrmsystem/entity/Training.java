package com.epam.gymcrmsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Training {
    private Long id;
    private String name;
    private Date trainingDate;
    private int duration;
    private Trainee trainee;
    private Trainer trainer;
    private TrainingType trainingType;
}