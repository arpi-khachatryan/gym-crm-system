package com.epam.gymcrmsystem.service;

import com.epam.gymcrmsystem.entity.Training;

import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TrainingService {
    Training createTraining(Training training);

    Training getTraining(Long id);

    List<Training> getAllTrainings();
}