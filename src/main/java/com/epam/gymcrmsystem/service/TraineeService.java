package com.epam.gymcrmsystem.service;

import com.epam.gymcrmsystem.entity.Trainee;

import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TraineeService {
    Trainee createTrainee(Trainee trainee);

    Trainee updateTrainee(Trainee trainee);

    boolean deleteTrainee(Long id);

    Trainee getTrainee(Long id);

    List<Trainee> getAllTrainees();
}