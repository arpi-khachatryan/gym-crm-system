package com.epam.gymcrmsystem.service.serviceImpl;

import com.epam.gymcrmsystem.dao.TraineeDao;
import com.epam.gymcrmsystem.dao.TrainerDao;
import com.epam.gymcrmsystem.dao.TrainingDao;
import com.epam.gymcrmsystem.entity.Trainee;
import com.epam.gymcrmsystem.entity.Trainer;
import com.epam.gymcrmsystem.entity.Training;
import com.epam.gymcrmsystem.service.TrainingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@Service
public class TrainingServiceImpl implements TrainingService {

    @Autowired
    private TrainingDao trainingDao;

    @Autowired
    private TraineeDao traineeDao;

    @Autowired
    private TrainerDao trainerDao;

    /**
     * Creates a new training.
     * 1. Retrieves the trainee and trainer based on their IDs.
     * 2. Checks if both the trainee and trainer exist.
     * 3. Checks if a training session already exists for the given trainee and trainer.
     * 4. If everything is valid, associates the trainee and trainer with the training and saves it.
     */
    @Override
    public Training createTraining(Training training) {
        Long trainerId = training.getTrainer().getId();
        Long traineeId = training.getTrainee().getId();

        Trainee trainee = traineeDao.findById(traineeId).orElse(null);
        Trainer trainer = trainerDao.findById(trainerId).orElse(null);

        if (trainee == null || trainer == null) {
            log.warn("Trainee with ID {} or Trainer with ID {} does not exist", traineeId, trainerId);
            return null;
        } else if (trainingDao.existsByTrainerIdAndTraineeId(trainerId, traineeId)) {
            log.warn("Training already exists for trainer with ID {} and trainee with ID {}", trainerId, traineeId);
            return null;
        } else {
            training.setTrainee(trainee);
            training.setTrainer(trainer);

            log.info("Creating new training: {}", training);
            return trainingDao.save(training);
        }
    }

    /**
     * Retrieves an existing training.
     */
    @Override
    public Training getTraining(Long id) {
        log.info("Retrieving training with id: {}", id);
        Optional<Training> trainingOptional = trainingDao.findById(id);
        if (trainingOptional.isPresent()) {
            Training training = trainingOptional.get();
            log.info("Retrieved training: {}", training);
            return training;
        } else {
            log.warn("Training with id {} not found", id);
            return null;
        }
    }

    /**
     * Retrieves all existing trainings.
     */
    @Override
    public List<Training> getAllTrainings() {
        log.info("Retrieving all trainings");
        List<Training> trainings = trainingDao.findAll();
        log.info("Retrieved all trainings: {}", trainings);
        return trainings;
    }
}