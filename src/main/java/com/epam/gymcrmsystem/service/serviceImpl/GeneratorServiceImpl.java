package com.epam.gymcrmsystem.service.serviceImpl;

import com.epam.gymcrmsystem.dao.UserDao;
import com.epam.gymcrmsystem.service.GeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.stream.Collectors;

/**
 * @author Arpi Khachatryan on 19.05.2024
 */

@Slf4j
@Service
public class GeneratorServiceImpl implements GeneratorService {

    @Autowired
    private UserDao userDao;

    /**
     * Calculates the base username using the user's first name and last name.
     */
    public String calculateBaseUserName(String firstName, String lastName) {
        String baseUserName = firstName + "." + lastName;
        log.debug("Calculated base username: {}", baseUserName);
        return baseUserName;
    }

    /**
     * Generates a unique username based on the base username.
     */
    public String generateUniqueUserName(String baseUserName) {
        String userName = baseUserName;
        int suffix = 1;
        while (userDao.existsByUsername(userName)) {
            userName = baseUserName + suffix;
            suffix++;
        }
        log.debug("Generated unique username: {}", userName);
        return userName;
    }

    /**
     * Generates a random password as a 10-character string.
     */
    public String generateRandomPassword() {
        String password = new SecureRandom().ints(10, 33, 122)
                .mapToObj(x -> String.valueOf((char) x))
                .collect(Collectors.joining());
        log.debug("Generated random password: {}", password);
        return password;
    }
}