package com.epam.gymcrmsystem.service.serviceImpl;

import com.epam.gymcrmsystem.dao.TrainerDao;
import com.epam.gymcrmsystem.dao.UserDao;
import com.epam.gymcrmsystem.entity.Trainer;
import com.epam.gymcrmsystem.entity.User;
import com.epam.gymcrmsystem.service.GeneratorService;
import com.epam.gymcrmsystem.service.TrainerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@Service
public class TrainerServiceImpl implements TrainerService {

    @Autowired
    private TrainerDao trainerDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GeneratorService generatorService;

    /**
     * Creates a new trainer.
     * 1.Generating username and password for the trainer's user account
     * 2.Saving the user and associating it with the trainer
     */
    @Override
    public Trainer createTrainer(Trainer trainer) {
        log.info("Creating new trainer: {}", trainer);
        User user = trainer.getUser();
        String baseUserName = generatorService.calculateBaseUserName(user.getFirstName(), user.getLastName());
        String userName = generatorService.generateUniqueUserName(baseUserName);
        String password = generatorService.generateRandomPassword();
        user.setUserName(userName);
        user.setPassword(password);
        user.setActive(true);
        userDao.save(user);
        trainer.setUser(user);
        Trainer createdTrainer = trainerDao.save(trainer);
        log.info("Created trainer: {}", createdTrainer);
        return createdTrainer;
    }

    /**
     * Updates an existing trainer.
     * 1.Checking if the trainer ID is valid
     * 2.Retrieving the existing trainer
     * 3.Generating and updating username if necessary
     * 4.Saving the updated user and associating it with the trainer
     */
    @Override
    public Trainer updateTrainer(Trainer trainer) {
        log.info("Updating trainer: {}", trainer);
        if (trainer.getId() == null) {
            log.warn("Trainer ID is null. Cannot update trainer without a valid ID");
            return null;
        }

        Trainer existingTrainer = trainerDao.findById(trainer.getId()).orElse(null);
        if (existingTrainer == null) {
            log.warn("Trainer with ID {} not found", trainer.getId());
            return null;
        }

        User user = trainer.getUser();
        if (!user.getFirstName().equals(existingTrainer.getUser().getFirstName()) ||
                !user.getLastName().equals(existingTrainer.getUser().getLastName())) {
            String userName = generatorService.generateUniqueUserName(generatorService.calculateBaseUserName(user.getFirstName(), user.getLastName()));
            user.setUserName(userName);
        }

        userDao.save(user);
        trainer.setUser(user);

        Trainer updatedTrainer = trainerDao.update(trainer);
        log.info("Updated trainer: {}", updatedTrainer);
        return updatedTrainer;
    }

    /**
     * Retrieves an existing trainer.
     */
    @Override
    public Trainer getTrainer(Long id) {
        log.info("Retrieving trainer with id: {}", id);
        Optional<Trainer> trainerOptional = trainerDao.findById(id);
        if (trainerOptional.isPresent()) {
            Trainer trainer = trainerOptional.get();
            log.info("Retrieved trainer: {}", trainer);
            return trainer;
        } else {
            log.warn("Trainer with id {} not found", id);
            return null;
        }
    }

    /**
     * Retrieves all existing trainers.
     */
    @Override
    public List<Trainer> getAllTrainers() {
        log.info("Retrieving all trainers");
        List<Trainer> trainers = trainerDao.findAll();
        log.info("Retrieved all trainers: {}", trainers);
        return trainers;
    }
}