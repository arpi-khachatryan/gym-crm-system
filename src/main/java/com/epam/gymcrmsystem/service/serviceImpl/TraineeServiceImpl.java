package com.epam.gymcrmsystem.service.serviceImpl;

import com.epam.gymcrmsystem.dao.TraineeDao;
import com.epam.gymcrmsystem.dao.UserDao;
import com.epam.gymcrmsystem.entity.Trainee;
import com.epam.gymcrmsystem.entity.User;
import com.epam.gymcrmsystem.service.GeneratorService;
import com.epam.gymcrmsystem.service.TraineeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@Service
public class TraineeServiceImpl implements TraineeService {

    @Autowired
    private TraineeDao traineeDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GeneratorService generatorService;

    /**
     * Creates a new trainee.
     * 1.Generating username and password for the trainee's user account
     * 2.Saving the user and associating it with the trainee
     */
    @Override
    public Trainee createTrainee(Trainee trainee) {
        log.info("Creating new trainee: {}", trainee);
        User user = trainee.getUser();
        String baseUserName = generatorService.calculateBaseUserName(user.getFirstName(), user.getLastName());
        String userName = generatorService.generateUniqueUserName(baseUserName);
        String password = generatorService.generateRandomPassword();
        user.setUserName(userName);
        user.setPassword(password);
        user.setActive(true);
        userDao.save(user);
        trainee.setUser(user);
        Trainee createdTrainee = traineeDao.save(trainee);
        log.info("Created trainee: {}", createdTrainee);
        return createdTrainee;
    }

    /**
     * Updates an existing trainee.
     * 1.Checking if the trainee ID is valid
     * 2.Retrieving the existing trainee
     * 3.Generating and updating username if necessary
     * 4.Saving the updated user and associating it with the trainee
     */
    @Override
    public Trainee updateTrainee(Trainee trainee) {
        log.info("Updating trainee: {}", trainee);
        if (trainee.getId() == null) {
            log.warn("Trainee ID is null. Cannot update trainee without a valid ID");
            return null;
        }
        Trainee existingTrainee = traineeDao.findById(trainee.getId()).orElse(null);
        if (existingTrainee == null) {
            log.warn("Trainee with ID {} not found", trainee.getId());
            return null;
        }

        User user = trainee.getUser();
        if (!user.getFirstName().equals(existingTrainee.getUser().getFirstName()) ||
                !user.getLastName().equals(existingTrainee.getUser().getLastName())) {
            String userName = generatorService.generateUniqueUserName(generatorService.calculateBaseUserName(user.getFirstName(), user.getLastName()));
            user.setUserName(userName);
        }

        userDao.save(user);
        trainee.setUser(user);

        Trainee updatedTrainee = traineeDao.update(trainee);
        log.info("Updated trainee: {}", updatedTrainee);
        return updatedTrainee;
    }

    /**
     * Deletes an existing trainee.
     */
    @Override
    public boolean deleteTrainee(Long id) {
        log.info("Deleting trainee with id: {}", id);
        Trainee trainee = traineeDao.findById(id).orElse(null);
        if (trainee == null) {
            log.warn("Trainee with id {} not found", id);
            return false;
        }
        traineeDao.delete(id);
        log.info("Trainee with id {} deleted successfully", id);
        return true;
    }

    /**
     * Retrieves an existing trainee.
     */
    @Override
    public Trainee getTrainee(Long id) {
        log.info("Retrieving trainee with id: {}", id);
        Optional<Trainee> traineeOptional = traineeDao.findById(id);
        if (traineeOptional.isPresent()) {
            Trainee trainee = traineeOptional.get();
            log.info("Retrieved trainee: {}", trainee);
            return trainee;
        } else {
            log.warn("Trainee with id {} not found", id);
            return null;
        }
    }

    /**
     * Retrieves all existing trainees.
     */
    @Override
    public List<Trainee> getAllTrainees() {
        log.info("Retrieving all trainees");
        List<Trainee> trainees = traineeDao.findAll();
        log.info("Retrieved all trainees: {}", trainees);
        return trainees;
    }
}