package com.epam.gymcrmsystem.service;

import com.epam.gymcrmsystem.entity.Trainer;

import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TrainerService {
    Trainer createTrainer(Trainer trainer);

    Trainer updateTrainer(Trainer trainer);

    Trainer getTrainer(Long id);

    List<Trainer> getAllTrainers();
}