package com.epam.gymcrmsystem.config;

import com.epam.gymcrmsystem.storage.TraineeStorage;
import com.epam.gymcrmsystem.storage.TrainerStorage;
import com.epam.gymcrmsystem.storage.TrainingStorage;
import com.epam.gymcrmsystem.storage.UserStorage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Configuration
@ComponentScan(basePackages = "com.epam.gymcrmsystem")
@PropertySource("classpath:application.yml")
public class Config {

    @Bean
    public TraineeStorage traineeStorage() {
        return new TraineeStorage();
    }

    @Bean
    public TrainerStorage trainerStorage() {
        return new TrainerStorage();
    }

    @Bean
    public TrainingStorage trainingStorage() {
        return new TrainingStorage();
    }

    @Bean
    public UserStorage userStorage() {
        return new UserStorage();
    }
}
