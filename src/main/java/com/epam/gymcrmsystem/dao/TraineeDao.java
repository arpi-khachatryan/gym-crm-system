package com.epam.gymcrmsystem.dao;

import com.epam.gymcrmsystem.entity.Trainee;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TraineeDao {
    Trainee save(Trainee trainee);

    Trainee update(Trainee trainee);

    void delete(Long id);

    Optional<Trainee> findById(Long id);

    List<Trainee> findAll();
}