package com.epam.gymcrmsystem.dao.daoImpl;

import com.epam.gymcrmsystem.dao.TrainerDao;
import com.epam.gymcrmsystem.entity.Trainer;
import com.epam.gymcrmsystem.storage.TrainerStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Repository
public class TrainerDaoImpl implements TrainerDao {

    private TrainerStorage trainerStorage;

    @Autowired
    public void setTrainerStorage(TrainerStorage trainerStorage) {
        this.trainerStorage = trainerStorage;
    }

    @Override
    public Trainer save(Trainer trainer) {
        return trainerStorage.save(trainer);
    }

    @Override
    public Trainer update(Trainer trainer) {
        return trainerStorage.update(trainer);
    }

    @Override
    public Optional<Trainer> findById(Long id) {
        return trainerStorage.findById(id);
    }

    @Override
    public List<Trainer> findAll() {
        return trainerStorage.findAll();
    }
}