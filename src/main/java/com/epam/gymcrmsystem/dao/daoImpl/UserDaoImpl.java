package com.epam.gymcrmsystem.dao.daoImpl;

import com.epam.gymcrmsystem.dao.UserDao;
import com.epam.gymcrmsystem.entity.User;
import com.epam.gymcrmsystem.storage.UserStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Arpi Khachatryan on 19.05.2024
 */

@Repository
public class UserDaoImpl implements UserDao {

    private UserStorage userStorage;

    @Autowired
    public void setUserStorage(UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    @Override
    public User save(User user) {
        return userStorage.save(user);
    }

    @Override
    public boolean existsByUsername(String userName) {
        return userStorage.existsByUsername(userName);
    }
}