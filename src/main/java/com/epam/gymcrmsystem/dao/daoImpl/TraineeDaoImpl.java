package com.epam.gymcrmsystem.dao.daoImpl;

import com.epam.gymcrmsystem.dao.TraineeDao;
import com.epam.gymcrmsystem.entity.Trainee;
import com.epam.gymcrmsystem.storage.TraineeStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Repository
public class TraineeDaoImpl implements TraineeDao {

    private TraineeStorage traineeStorage;

    @Autowired
    public void setTraineeStorage(TraineeStorage traineeStorage) {
        this.traineeStorage = traineeStorage;
    }

    @Override
    public Trainee save(Trainee trainee) {
        return traineeStorage.save(trainee);
    }

    @Override
    public Trainee update(Trainee trainee) {
        return traineeStorage.update(trainee);
    }

    @Override
    public void delete(Long id) {
        traineeStorage.delete(id);
    }

    @Override
    public Optional<Trainee> findById(Long id) {
        return traineeStorage.findById(id);
    }

    @Override
    public List<Trainee> findAll() {
        return traineeStorage.findAll();
    }
}