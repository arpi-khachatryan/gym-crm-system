package com.epam.gymcrmsystem.dao.daoImpl;

import com.epam.gymcrmsystem.dao.TrainingDao;
import com.epam.gymcrmsystem.entity.Training;
import com.epam.gymcrmsystem.storage.TrainingStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Repository
public class TrainingDaoImpl implements TrainingDao {

    private TrainingStorage trainingStorage;

    @Autowired
    public void setTrainingStorage(TrainingStorage trainingStorage) {
        this.trainingStorage = trainingStorage;
    }

    @Override
    public Training save(Training training) {
        return trainingStorage.save(training);
    }

    @Override
    public Optional<Training> findById(Long id) {
        return trainingStorage.findById(id);
    }

    @Override
    public List<Training> findAll() {
        return trainingStorage.findAll();
    }

    @Override
    public boolean existsByTrainerIdAndTraineeId(Long trainerId, Long traineeId) {
        return trainingStorage.existsByTrainerIdAndTraineeId(trainerId, traineeId);
    }
}