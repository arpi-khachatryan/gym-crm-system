package com.epam.gymcrmsystem.dao;

import com.epam.gymcrmsystem.entity.User;

/**
 * @author Arpi Khachatryan on 19.05.2024
 */

public interface UserDao {
    User save(User user);

    boolean existsByUsername(String userName);
}