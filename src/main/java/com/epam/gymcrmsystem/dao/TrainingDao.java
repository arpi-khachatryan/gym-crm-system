package com.epam.gymcrmsystem.dao;

import com.epam.gymcrmsystem.entity.Training;

import java.util.List;
import java.util.Optional;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

public interface TrainingDao {
    Training save(Training training);

    Optional<Training> findById(Long id);

    List<Training> findAll();

    boolean existsByTrainerIdAndTraineeId(Long trainerId, Long traineeId);
}