package com.epam.gymcrmsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GymCrmSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(GymCrmSystemApplication.class, args);
    }

}