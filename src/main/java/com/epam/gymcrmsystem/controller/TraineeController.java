package com.epam.gymcrmsystem.controller;

import com.epam.gymcrmsystem.entity.Trainee;
import com.epam.gymcrmsystem.service.TraineeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/trainees")
public class TraineeController {

    private final TraineeService traineeService;

    @Autowired
    public TraineeController(TraineeService traineeService) {
        this.traineeService = traineeService;
    }

    @PostMapping
    public ResponseEntity<Trainee> createTrainee(@RequestBody Trainee trainee) {
        log.info("Creating new trainee: {}", trainee);
        Trainee createdTrainee = traineeService.createTrainee(trainee);
        log.info("Created trainee: {}", createdTrainee);
        return new ResponseEntity<>(createdTrainee, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Trainee> updateTrainee(@RequestBody Trainee trainee) {
        log.info("Updating trainee: {}", trainee);
        Trainee updatedTrainee = traineeService.updateTrainee(trainee);
        if (updatedTrainee == null) {
            log.warn("Trainee with id {} not found", trainee.getId());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Updated trainee: {}", updatedTrainee);
        return new ResponseEntity<>(updatedTrainee, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTrainee(@PathVariable Long id) {
        log.info("Deleting trainee with id: {}", id);
        boolean isDeleted = traineeService.deleteTrainee(id);
        if (!isDeleted) {
            log.warn("Trainee with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Deleted trainee with id: {}", id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Trainee> getTrainee(@PathVariable Long id) {
        log.info("Retrieving trainee with id: {}", id);
        Trainee trainee = traineeService.getTrainee(id);
        if (trainee == null) {
            log.warn("Trainee with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Retrieved trainee: {}", trainee);
        return new ResponseEntity<>(trainee, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Trainee>> getAllTrainees() {
        log.info("Retrieving all trainees");
        List<Trainee> trainees = traineeService.getAllTrainees();
        if (trainees == null) {
            log.warn("Trainees not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Retrieved all trainees: {}", trainees);
        return new ResponseEntity<>(trainees, HttpStatus.OK);
    }
}