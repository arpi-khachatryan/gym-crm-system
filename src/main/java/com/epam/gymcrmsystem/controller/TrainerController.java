package com.epam.gymcrmsystem.controller;

import com.epam.gymcrmsystem.entity.Trainer;
import com.epam.gymcrmsystem.service.TrainerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/trainers")
public class TrainerController {

    private final TrainerService trainerService;

    @Autowired
    public TrainerController(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    @PostMapping
    public ResponseEntity<Trainer> createTrainer(@RequestBody Trainer trainer) {
        log.info("Creating new trainer: {}", trainer);
        Trainer createdTrainer = trainerService.createTrainer(trainer);
        log.info("Created trainer: {}", createdTrainer);
        return new ResponseEntity<>(createdTrainer, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Trainer> updateTrainer(@RequestBody Trainer trainer) {
        log.info("Updating trainer: {}", trainer);
        Trainer updatedTrainer = trainerService.updateTrainer(trainer);
        if (updatedTrainer == null) {
            log.warn("Trainer with id {} not found", trainer.getId());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Updated trainer: {}", updatedTrainer);
        return new ResponseEntity<>(updatedTrainer, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Trainer> getTrainer(@PathVariable Long id) {
        log.info("Retrieving trainer with id: {}", id);
        Trainer trainer = trainerService.getTrainer(id);
        if (trainer == null) {
            log.warn("Trainer with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Retrieved trainer: {}", trainer);
        return new ResponseEntity<>(trainer, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Trainer>> getAllTrainers() {
        log.info("Retrieving all trainers");
        List<Trainer> trainers = trainerService.getAllTrainers();
        if (trainers == null) {
            log.warn("trainers not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Retrieved all trainers: {}", trainers);
        return new ResponseEntity<>(trainers, HttpStatus.OK);
    }
}