package com.epam.gymcrmsystem.controller;

import com.epam.gymcrmsystem.entity.Training;
import com.epam.gymcrmsystem.service.TrainingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Arpi Khachatryan on 18.05.2024
 */

@Slf4j
@RestController
@RequestMapping("/trainings")
public class TrainingController {

    private final TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @PostMapping
    public ResponseEntity<?> createTraining(@RequestBody Training training) {
        log.info("Creating new training: {}", training);
        Training createdTraining = trainingService.createTraining(training);

        if (createdTraining != null) {
            log.info("Created training: {}", createdTraining);
            return new ResponseEntity<>(createdTraining, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Failed to create training. Please check the provided data.", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Training> getTraining(@PathVariable Long id) {
        log.info("Retrieving training with id: {}", id);
        Training training = trainingService.getTraining(id);
        if (training == null) {
            log.warn("Training with id {} not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Retrieved training: {}", training);
        return new ResponseEntity<>(training, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Training>> getAllTrainings() {
        log.info("Retrieving all trainings");
        List<Training> trainings = trainingService.getAllTrainings();
        if (trainings == null) {
            log.warn("Trainings not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Retrieved all trainings: {}", trainings);
        return new ResponseEntity<>(trainings, HttpStatus.OK);
    }
}