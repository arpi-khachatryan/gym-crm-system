package com.epam.gymcrmsystem.service.seriveImpl;

import com.epam.gymcrmsystem.dao.UserDao;
import com.epam.gymcrmsystem.service.serviceImpl.GeneratorServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.epam.gymcrmsystem.parametrs.MockData.getUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * @author Arpi Khachatryan on 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class GeneratorServiceImplTest {

    @InjectMocks
    private GeneratorServiceImpl generatorService;

    @Mock
    private UserDao userDao;

    @Test
    public void calculateBaseUserName() {
        String firstName = getUser().getFirstName();
        String lastName = getUser().getLastName();

        String baseUserName = firstName + "." + lastName;

        assertEquals("George.Smith", baseUserName);
    }

    @Test
    public void generateUniqueUserName() {
        String baseUserName = "George.Smith";
        int suffix = 1;

        when(userDao.existsByUsername(baseUserName)).thenReturn(true);
        when(userDao.existsByUsername(baseUserName + suffix)).thenReturn(false);
        String uniqueUserName = generatorService.generateUniqueUserName(baseUserName);

        assertEquals(baseUserName + suffix, uniqueUserName);
    }

    @Test
    public void generateRandomPassword() {
        String password = generatorService.generateRandomPassword();

        assertNotNull(password);
        assertEquals(10, password.length());
    }
}