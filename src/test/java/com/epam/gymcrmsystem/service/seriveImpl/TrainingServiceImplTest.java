package com.epam.gymcrmsystem.service.seriveImpl;

import com.epam.gymcrmsystem.dao.TraineeDao;
import com.epam.gymcrmsystem.dao.TrainerDao;
import com.epam.gymcrmsystem.dao.TrainingDao;
import com.epam.gymcrmsystem.entity.Training;
import com.epam.gymcrmsystem.service.serviceImpl.TrainingServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.epam.gymcrmsystem.parametrs.MockData.getTraining;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan on 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TrainingServiceImplTest {

    @InjectMocks
    private TrainingServiceImpl trainingService;

    @Mock
    private TrainingDao trainingDao;

    @Mock
    private TraineeDao traineeDao;

    @Mock
    private TrainerDao trainerDao;

    @Test
    public void createTraining() {
        Training training = getTraining();
        Long trainerId = training.getTrainer().getId();
        Long traineeId = training.getTrainee().getId();

        when(traineeDao.findById(traineeId)).thenReturn(Optional.of(training.getTrainee()));
        when(trainerDao.findById(trainerId)).thenReturn(Optional.of(training.getTrainer()));
        when(trainingDao.existsByTrainerIdAndTraineeId(trainerId, trainerId)).thenReturn(false);
        training.setTrainee(training.getTrainee());
        training.setTrainer(training.getTrainer());
        when(trainingDao.save(any(Training.class))).thenReturn(training);
        Training createdTraining = trainingService.createTraining(training);

        assertNotNull(createdTraining);
        verify(trainingDao).save(training);
    }

    @Test
    public void testGetTrainer() {
        Training training = getTraining();

        when(trainingDao.findById(training.getId())).thenReturn(Optional.of(training));
        Training result = trainingService.getTraining(training.getId());

        assertNotNull(result);
        assertEquals(training, result);
    }

    @Test
    public void testGetAllTrainers() {
        Training training = getTraining();
        List<Training> trainings = List.of(training, training, training);

        when(trainingDao.findAll()).thenReturn(trainings);
        List<Training> result = trainingService.getAllTrainings();

        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals(trainings, result);
    }
}