package com.epam.gymcrmsystem.service.seriveImpl;

import com.epam.gymcrmsystem.dao.TrainerDao;
import com.epam.gymcrmsystem.dao.UserDao;
import com.epam.gymcrmsystem.entity.Trainer;
import com.epam.gymcrmsystem.entity.User;
import com.epam.gymcrmsystem.service.GeneratorService;
import com.epam.gymcrmsystem.service.serviceImpl.TrainerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.epam.gymcrmsystem.parametrs.MockData.getTrainer;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan on 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TrainerServiceImplTest {

    @InjectMocks
    private TrainerServiceImpl trainerService;

    @Mock
    private TrainerDao trainerDao;

    @Mock
    private UserDao userDao;

    @Mock
    private GeneratorService generatorService;

    @Test
    public void createTrainer() {
        Trainer trainer = getTrainer();
        User user = trainer.getUser();

        when(generatorService.calculateBaseUserName(user.getFirstName(), user.getLastName())).thenReturn("george.smith");
        when(generatorService.generateUniqueUserName("george.smith")).thenReturn("george.smith");
        when(generatorService.generateRandomPassword()).thenReturn("password1234");
        user.setUserName("george.smith");
        user.setPassword("password1234");
        user.setActive(true);
        when(userDao.save(any(User.class))).thenReturn(user);
        when(trainerDao.save(any(Trainer.class))).thenReturn(trainer);
        Trainer createdTrainer = trainerService.createTrainer(trainer);

        assertNotNull(createdTrainer);
        assertEquals("george.smith", createdTrainer.getUser().getUserName());
        assertEquals("password1234", createdTrainer.getUser().getPassword());
        assertTrue(createdTrainer.getUser().isActive());
        verify(generatorService).calculateBaseUserName(user.getFirstName(), user.getLastName());
        verify(generatorService).generateUniqueUserName("george.smith");
        verify(generatorService).generateRandomPassword();
        verify(userDao).save(user);
        verify(trainerDao).save(trainer);
    }

    @Test
    public void testUpdateTrainer() {
        Trainer trainer = getTrainer();
        Trainer updatedTrainer = getTrainer();
        updatedTrainer.setSpecialization("Yoga");

        when(trainerDao.findById(trainer.getId())).thenReturn(Optional.of(trainer));
        when(trainerDao.update(updatedTrainer)).thenReturn(updatedTrainer);
        Trainer result = trainerService.updateTrainer(updatedTrainer);

        assertNotNull(result);
        assertEquals(updatedTrainer, result);
    }

    @Test
    public void testGetTrainer() {
        Trainer trainer = getTrainer();

        when(trainerDao.findById(trainer.getId())).thenReturn(Optional.of(trainer));
        Trainer result = trainerService.getTrainer(trainer.getId());

        assertNotNull(result);
        assertEquals(trainer, result);
    }

    @Test
    public void testGetAllTrainers() {
        Trainer trainer = getTrainer();
        List<Trainer> trainers = List.of(trainer, trainer, trainer);

        when(trainerDao.findAll()).thenReturn(trainers);
        List<Trainer> result = trainerService.getAllTrainers();

        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals(trainers, result);
    }
}