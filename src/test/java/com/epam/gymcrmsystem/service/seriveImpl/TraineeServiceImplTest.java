package com.epam.gymcrmsystem.service.seriveImpl;

import com.epam.gymcrmsystem.dao.TraineeDao;
import com.epam.gymcrmsystem.dao.UserDao;
import com.epam.gymcrmsystem.entity.Trainee;
import com.epam.gymcrmsystem.entity.User;
import com.epam.gymcrmsystem.service.GeneratorService;
import com.epam.gymcrmsystem.service.serviceImpl.TraineeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.epam.gymcrmsystem.parametrs.MockData.getTrainee;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Arpi Khachatryan on 21.05.2024
 */

@ExtendWith(MockitoExtension.class)
public class TraineeServiceImplTest {

    @InjectMocks
    private TraineeServiceImpl traineeService;

    @Mock
    private TraineeDao traineeDao;

    @Mock
    private UserDao userDao;

    @Mock
    private GeneratorService generatorService;

    @Test
    public void createTrainee() {
        Trainee trainee = getTrainee();
        User user = trainee.getUser();

        when(generatorService.calculateBaseUserName(user.getFirstName(), user.getLastName())).thenReturn("george.smith");
        when(generatorService.generateUniqueUserName("george.smith")).thenReturn("george.smith");
        when(generatorService.generateRandomPassword()).thenReturn("password1234");
        user.setUserName("george.smith");
        user.setPassword("password1234");
        user.setActive(true);
        when(userDao.save(any(User.class))).thenReturn(user);
        when(traineeDao.save(any(Trainee.class))).thenReturn(trainee);
        Trainee createdTrainee = traineeService.createTrainee(trainee);

        assertNotNull(createdTrainee);
        assertEquals("george.smith", createdTrainee.getUser().getUserName());
        assertEquals("password1234", createdTrainee.getUser().getPassword());
        assertTrue(createdTrainee.getUser().isActive());
        verify(generatorService).calculateBaseUserName(user.getFirstName(), user.getLastName());
        verify(generatorService).generateUniqueUserName("george.smith");
        verify(generatorService).generateRandomPassword();
        verify(userDao).save(user);
        verify(traineeDao).save(trainee);
    }

    @Test
    public void testUpdateTrainee() {
        Trainee trainee = getTrainee();
        Trainee updatedTrainee = getTrainee();
        updatedTrainee.setAddress("Nalbandyan St");

        when(traineeDao.findById(trainee.getId())).thenReturn(Optional.of(trainee));
        when(traineeDao.update(updatedTrainee)).thenReturn(updatedTrainee);
        Trainee result = traineeService.updateTrainee(updatedTrainee);

        assertNotNull(result);
        assertEquals(updatedTrainee, result);
    }

    @Test
    public void testDeleteTrainee() {
        when(traineeDao.findById(getTrainee().getId())).thenReturn(Optional.of(getTrainee()));
        doNothing().when(traineeDao).delete(getTrainee().getId());
        boolean result = traineeService.deleteTrainee(getTrainee().getId());

        assertTrue(result);
    }

    @Test
    public void testGetTrainee() {
        Trainee trainee = getTrainee();

        when(traineeDao.findById(trainee.getId())).thenReturn(Optional.of(trainee));
        Trainee result = traineeService.getTrainee(trainee.getId());

        assertNotNull(result);
        assertEquals(trainee, result);
    }

    @Test
    public void testGetAllTrainees() {
        Trainee trainee = getTrainee();
        List<Trainee> trainees = List.of(trainee, trainee, trainee);

        when(traineeDao.findAll()).thenReturn(trainees);
        List<Trainee> result = traineeService.getAllTrainees();

        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals(trainees, result);
    }
}