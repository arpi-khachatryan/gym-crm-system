package com.epam.gymcrmsystem.parametrs;

import com.epam.gymcrmsystem.entity.*;

import java.util.Date;

public class MockData {

    public static User getUser() {
        return User.builder()
                .id(1L)
                .firstName("George")
                .lastName("Smith")
                .userName("george.smith")
                .password("password1234")
                .isActive(true)
                .build();
    }

    public static User getTraineeUser() {
        return User.builder()
                .id(1L)
                .firstName("George")
                .lastName("Smith")
                .build();
    }

    public static User getTrainerUser() {
        return User.builder()
                .id(2L)
                .firstName("George")
                .lastName("Smith")
                .userName("george.smith")
                .password("paswword1234")
                .isActive(true)
                .build();
    }

    public static Trainee getTrainee() {
        return Trainee.builder()
                .id(1L)
                .address("Tumanyan St")
                .dateOfBirth(new Date(2024, 05, 21))
                .user(getTraineeUser())
                .build();
    }

    public static Trainer getTrainer() {
        return Trainer.builder()
                .id(1L)
                .specialization("Fitness")
                .user(getTrainerUser())
                .build();
    }

    public static TrainingType getTrainingType() {
        return TrainingType.ENDURANCE;
    }

    public static Training getTraining() {
        return Training.builder()
                .id(1L)
                .name("Morning Workout")
                .trainingDate(new Date(2024, 05, 21))
                .duration(12)
                .trainee(getTrainee())
                .trainer(getTrainer())
                .trainingType(getTrainingType())
                .build();
    }
}