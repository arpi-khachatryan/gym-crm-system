[gym-crm-system-spring-core](https://gitlab.com/arpi-khachatryan/gym-crm-system-spring) - This project utilizes Spring Core without Spring Boot.

# gym-CRM-system

The Gym CRM System is a Spring-based application designed to manage gym memberships, trainers, and training sessions efficiently. It provides a set of services for creating, updating, and deleting profiles of trainees and trainers, as well as managing training sessions.


